terraform { 
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    encrypt = true
    # cannot have substitution at this point
    bucket = "tf.noelsr.dev"
    # cannot have substitution at this point
    region = "eu-central-1"
    # cannot have substitution at this point
    key = "noelsr.dev-cloudflare/terraform.tfstate"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_prefix}.${var.domain}"
  acl    = "public-read"
  policy = <<POLICY
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "PublicReadGetObject",
			"Effect": "Allow",
			"Principal": "*",
			"Action": "s3:GetObject",
			"Resource": "arn:aws:s3:::${var.bucket_prefix}.${var.domain}/*"
		}
	]
}
POLICY
  website {
    index_document = var.index_document
    error_document = var.error_document
  }
  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

}


provider "cloudflare" {
  email = var.email
  api_token = var.api_token
}


resource "cloudflare_record" "bucket_name" {
  zone_id = var.zone_id
  name    = "${var.bucket_prefix}.${var.domain}"
  value   = "${var.bucket_prefix}.${var.domain}.s3-website.${var.region}.amazonaws.com"
  type    = "CNAME"
  proxied = true
}
