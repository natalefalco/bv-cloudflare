
variable "region" {
  type    = string
  description = "aws region to build in"
  default = "eu-central-1"
}
variable "tf_bucket" {
  type    = string
  description = "bucket to store tfstate"
  default = "tf.noelsr.dev"
}
variable "bucket_prefix" {
  type    = string
  description = "aws bucket prefix to deploy in, should not contain dots"
  default = "example_bucket_name" 
}
variable "index_document" {
  type    = string
  description = "index page in bucket"
  default = "index.html"
}
variable "error_document" {
  type    = string
  description = "error page in bucket"
  default = "error.html"
}
variable "email" { 
  type    = string
  description = "cloudflare email"
  default = "i_wont@share.here"
}
variable "api_token" { 
  type    = string
  description = "cloudflare token for dns change"
  default = "i_wont_hardcode_that_:)"
}
variable "domain" { 
  type    = string
  description = "use domain to deploy"
  default = "example.com"
}
variable "zone_id" { 
  type    = string
  description = "hexadecimal zoneid"
  default = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
}
