# Cloudflare hosting setup

The variables injected using env variables
the following should be set:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- TR_VAR_region
- TF_VAR_bucket_prefix
- TF_VAR_email
- TF_VAR_api_token
- TF_VAR_domain
- TF_VAR_zone_id

For forking change the tfstate storage since it still not supports variable substitution

